package at.spenger.jaxb;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.Text;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import at.spenger.jaxb.model.Book;
import at.spenger.jaxb.model.Bookstore;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application implements CommandLineRunner {

	private static final String BOOKSTORE_XML = "./bookstore-jaxb.xml";

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    private void schreiben() throws JAXBException {
    	ArrayList<Book> bookList = new ArrayList<Book>();

		// create books
		Book book1 = new Book();
		book1.setIsbn("978-0060554736");
		book1.setName("The Game");
		book1.setAuthor("Neil Strauss");
		book1.setPublisher("Harpercollins");
		bookList.add(book1);

		Book book2 = new Book();
		book2.setIsbn("978-3832180577");
		book2.setName("Feuchtgebiete");
		book2.setAuthor("Charlotte Roche");
		book2.setPublisher("Dumont Buchverlag");
		bookList.add(book2);

		// create bookstore, assigning book
		Bookstore bookstore = new Bookstore();
		bookstore.setName("Fraport Bookstore");
		bookstore.setLocation("Frankfurt Airport");
		bookstore.setBookList(bookList);

		// create JAXB context and instantiate marshaller
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		// Write to System.out
		m.marshal(bookstore, System.out);

		// Write to File
		m.marshal(bookstore, new File(BOOKSTORE_XML));

    }
    
    
    private void lesen() throws JAXBException, FileNotFoundException {
    	System.out.println("Output from our XML File: ");
		// create JAXB context and instantiate marshaller
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		Unmarshaller um = context.createUnmarshaller();
		Bookstore bookstore2 = (Bookstore) um.unmarshal(new FileReader(
				BOOKSTORE_XML));
		ArrayList<Book> list = bookstore2.getBooksList();
		list.forEach(b -> System.out.println("Book: " + b.getName() + " from "
					+ b.getAuthor()));
    }

	@Override
	public void run(String... arg0) throws Exception {
		System.out.println("-----schreiben-----");
		schreiben();
		System.out.println("-----lesen-----");
		lesen();
		System.out.println("-----schreiben-----");
		write();
		System.out.println("-----lesen-----");
		read();
		System.out.println("-----read und convert-----");
		readConvert();
		System.out.println("-----read-----");
		read();
	}
	
	private void write() throws IOException {
		JsonObject model = Json.createObjectBuilder()
				   .add("Ebene", "Info")
				   .add("Datum", "19.12.2013")
				   .add("Uhrzeit", "12:06:33")
				   .add("Quelle", "Microsoft Windows")
				   .add("Ereignis_id", "7")
				   .add("Aufgabenkategorie", "Error")
				   .build();
		
		FileWriter stWriter = new FileWriter("data.json");
		
		try (JsonWriter jsonWriter = Json.createWriter(stWriter)) {
		   jsonWriter.writeObject(model);
		}
	}
	
	private void read() throws IOException {
		FileInputStream fis = new FileInputStream("daten.json");
		JsonObject model = Json.createReader(fis).readObject();
		
		if(model != null)
			for(Entry<String, JsonValue> f : model.entrySet())
			{
				System.out.println(f.getKey()+ " --> "+f.getValue().toString());
			}
		
	}
	
	private void readConvert() throws IOException
	{
		BufferedReader r = new BufferedReader(new FileReader("daten.txt"));
		 
		
		
		ArrayList<String[]> list = new ArrayList<String[]>();
		ArrayList<JsonObject> jsonobjects = new ArrayList<JsonObject>();
		
		for(String b=r.readLine(); b != null; b =r.readLine())
		{
			list.add(b.split(","));
		}
		
		String[] fields = list.get(0);
		list.remove(0);
		
		for(String[] current : list)
		{
			JsonObject model = Json.createObjectBuilder()
				.add(fields[0], current[0])
				.add(fields[1], current[1])
				.add(fields[2], current[2])
				.add(fields[3], current[3])
				.add(fields[4], current[4])
				.build();
		
			jsonobjects.add(model);
		}
		//
		StringBuilder b = new StringBuilder();
		
		for(JsonObject o : jsonobjects)
		{
			b.append(o.toString()+"\n");
		}
		
		FileWriter f = new FileWriter("daten.json");
		f.write(b.toString());

		r.close();
		f.close();
	}
	
	
}
